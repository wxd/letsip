# LetSIP — Learning to Sample Interesting Patterns

This repository contains the implementation of the LetSIP algorithm:

Dzyuba, V., van Leeuwen, M. (2017). Learning what matters — Sampling interesting patterns. Proceedings of PAKDD  
[Conference version (Springer)](https://link.springer.com/chapter/10.1007%2F978-3-319-57454-7_42) | 
[Extended version (ArXiV)](https://arxiv.org/abs/1702.01975) | 
[Slides](http://pdf.vl-dz.net/papers/2017-pakdd-learning_what_matters__sampling_interesting_patterns-dzyuba_vanleeuwen-slides.pdf)

See [Vladimir Dzyuba's personal website](http://vl-dz.net/publications.html) for more related work.


## Running the demo scenario

Use Docker to run the demo scenario on the `zoo` dataset:

```bash
docker build -f Dockerfile -t letsip .
docker run --rm letsip
```

This will print the ranked queries, Stochastic Coordinate Descent statistics, and the learned feature weights.


## Reproducing the experiments or extending the code.

Please contact [Vladimir Dzyuba](http://vl-dz.net). 
