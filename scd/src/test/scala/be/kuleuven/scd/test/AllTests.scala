package be.kuleuven.scd.test

import org.junit.runner.RunWith
import org.scalatest.Suites
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class AllTests extends Suites(new CompareWithOriginalTest)
