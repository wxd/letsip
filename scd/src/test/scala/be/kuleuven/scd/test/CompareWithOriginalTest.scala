package be.kuleuven.scd.test

import be.kuleuven.scd._

import scala.io.Source
import scala.sys.process._
import scala.util.Random

class CompareWithOriginalTest extends ScdTest {
  private val binary = "SCD/scd"
  private val (loss, lossIndex) = (LogisticLoss, 0)

  private val datasets = Seq(("test", 3, 5), ("arcene", 100, 10000))
  private val iterations0 = Seq(1000, 20000)
  private val lambdas = Seq(0.1, 0.01, 0.001, 0.000001)

  for (((datasetName, m, d), iterations) <- datasets.zip(iterations0);
       datasetFile = s"SCD/$datasetName"; examples = load(datasetFile);
       lambda <- lambdas) {
    s"Updates for `$datasetName` and `lambda=$lambda`" should "match the updates of the orgiinal implementation" in {
      assert(examples.size == m && examples.head.length == d)

      val originalIterations = runOriginal(datasetFile, lambda, iterations)
      val fakeRnd = new NotRandom(d, originalIterations)

      val params = ScdParameters(FixedIterations(1), lambda) // Iterations are performed 'manually'
      Iterator.iterate(ColdStart.asInstanceOf[ScdState]) { s =>
        SCD.optimize(loss, examples, params, m = Some(m), d = Some(d), state = s)(fakeRnd)._2
      }.drop(1).take(iterations).zip(originalIterations.iterator).foreach {
        case (HotStart(w, z), IterationOutput(t, j, _, _, _, newWeight, weightNorm, nonZeroWeights, totalLoss, _)) =>
          assert(w(j) === newWeight +- 0.0001, s"Iteration $t, feature $j")
      }
    }
  }

  private def load(dataset: String): IndexedSeq[Example] = {
    val featureLines = Source.fromFile(featureFile(dataset)).getLines()
    val labelLines = Source.fromFile(labelFile(dataset)).getLines()

    val Array(m, d) = featureLines.next().split(" ").map(_.toInt)
    val examples = Iterator.fill(m)(Array.ofDim[Double](d))
      .zip(labelLines.map(_.toDouble))
      .toIndexedSeq
    featureLines.zipWithIndex.filter(_._1.nonEmpty).foreach { case (line, j) =>
      line.split(" ").grouped(2).foreach { case Array(i, xij) => examples(i.toInt)._1(j) = xij.toDouble }
    }

    examples.map { case (xi, yi) => new ArrayExample(xi, d, yi) }
  }

  private def featureFile(dataset:String) = s"${dataset}_examples"
  private def labelFile(dataset:String)   = s"${dataset}_labels"

  private def runOriginal(dataset: String, lambda: Double, iterations: Int): IndexedSeq[IterationOutput] =
    List(
      binary,
      "-loss", lossIndex.toString,
      "-lambda", lambda.toString,
      "-iters", iterations.toString,
      featureFile(dataset), labelFile(dataset)
    ).lineStream.drop(1).map { line =>
      val p = line.split(" ")
      IterationOutput(
        p(0).toInt, p(1).toInt, p(2).toDouble, p(3).toDouble, p(4).toDouble,
        p(5).toDouble, p(6).toDouble, p(7).toInt, p(8).toDouble, p(9).toDouble
      )
    }.toIndexedSeq

  private case class IterationOutput(t: Int,
                                     j: Int,
                                     oldWeight: Double,
                                     g: Double,
                                     eta: Double,
                                     newWeight: Double,
                                     weightNorm: Double,
                                     nonZeroWeights: Int,
                                     totalLoss: Double,
                                     regularizedLoss: Double)

  private class NotRandom(private val d: Int,
                          iterations: IndexedSeq[IterationOutput]) extends Random {
    private val indices = iterations.iterator.map(_.j)

    override def nextInt(n: Int): Int =
      if (n == d) indices.next() else throw new UnsupportedOperationException
  }
}
