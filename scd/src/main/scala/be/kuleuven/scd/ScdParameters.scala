package be.kuleuven.scd

final case class ScdParameters(iterations: IterationCalculator, lambda: Double = 0.000001)