FROM openjdk:8-jdk-slim AS builder

# First, copy the minimal amount of files to initialize the Gradle project
# so that changing the source code doesn't lead to the whole image being re-built
COPY gradlew build.gradle settings.gradle /letsip/
WORKDIR letsip

COPY gradle ./gradle
COPY lewits/build.gradle lewits/
COPY scd/build.gradle scd/

RUN ./gradlew --parallel --refresh-dependencies :lewits:dependencies

# Now, copy the source code and build the app
COPY . ./
RUN ./gradlew --parallel :lewits:installApp


FROM openjdk:8-jre-slim

COPY --from=builder /letsip/lewits/build/install/lewits /letsip
COPY --from=builder /letsip/lewits/logback.xml /letsip/bin/logback.xml
ENV LEWITS_OPTS="-Dlogback.configurationFile=/letsip/bin/logback.xml"
ADD https://dtai.cs.kuleuven.be/CP4IM/datasets/data/zoo-1.txt zoo-1.txt 

ENTRYPOINT /letsip/bin/lewits