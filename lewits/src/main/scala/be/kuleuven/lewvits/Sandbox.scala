package be.kuleuven.lewvits

import be.kuleuven.lewvits.feature.{Intercept, Items}
import be.kuleuven.pmlib.itemsets.{CP4IMData, CP4IMParameters}
import be.kuleuven.scd.{FixedIterations, ScdParameters}

object Sandbox extends App {
  private val datasetPath = if (args.size > 0) { args(0) } else { "zoo-1.txt" }
  private val dataset = CP4IMData.load(path = datasetPath, CP4IMParameters(dropLabel = true))
  println(dataset.name, dataset.size, dataset.attributes.size)
  
  if (!dataset.name.startsWith("zoo")) {
    println("The demo script expects the `zoo-1` CP4IM dataset: https://dtai.cs.kuleuven.be/CP4IM/datasets/data/zoo-1.txt")
    sys.exit(1)
  }

  private val minsup = 10
  private val ranker = FrequencyRanker
  private val features = feature.compose(Intercept, Items)
  private val scdParams = ScdParameters(iterations = FixedIterations(dataset.attributes.size))
  private val tilt = 10.0

  private val params = Parameters(tilt = tilt, featureMap = features, scd = scdParams, queryOverlap = 1, iterations = 30)
  private val learnedWeight = Lewvits.loop(dataset, minsup, ranker, params)
  (0 until learnedWeight.weights.length).view.map(learnedWeight.weights(_))
    .zip(dataset.attributes)
    .filter(_._1 != 0)
    .sortBy(-_._1.abs)
    .foreach { case (w, i) => println(f"${i.name}%15s = $w% .6f") }
}
