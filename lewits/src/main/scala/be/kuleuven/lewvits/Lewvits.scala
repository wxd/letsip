package be.kuleuven.lewvits

import be.kuleuven.lewvits.feature.{FeatureMap, FeatureMapGenerator}
import be.kuleuven.pmlib.data.Dataset
import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.scd._
import be.kuleuven.weightgen.{Uniform, WeightFunction, WeightGenConfiguration, WeightMcConfiguration}
import be.kuleuven.wg4ps
import be.kuleuven.wg4ps.specialised.EclatProblem
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

import scala.annotation.tailrec
import scala.util.Random

object Lewvits extends LewvitsLogging {
  protected override val logger = Logger(LoggerFactory.getLogger("MILeR"))

  private val countingConf = WeightMcConfiguration.default
  private val samplingConf = WeightGenConfiguration(kappa = 0.9)

  def loop(dataset: Dataset[Set[Int]],
           minsup: Int,
           client: Ranker,
           params: Parameters,
           sampler: EFlexics = VanillaEFlexics,
           seed: Long = System.nanoTime().hashCode()): LogisticWeight = {
    val sampler0 = EFlexicsWrapper(sampler)
    logTask(dataset, minsup, client, params, sampler0, seed)

    val miningProblem = EclatProblem(dataset, minsup)
    val featureMap = params.featureMap match {
      case generator: FeatureMapGenerator => generator.toFeatureMap(dataset)
      case map: FeatureMap => map
    }
    val rnd = new Random(seed)

    val queries = Array.ofDim[Itemset](params.iterations, params.querySize)
    val trainingPairs = Array.ofDim[RankedPair](params.iterations * params.pairsPerQuery)

    val loss = LogisticLoss
    val d = featureMap.featureCount
    val w = ArrayVector.zeros(d)
    val z = ArrayVector.zeros(params.iterations * params.pairsPerQuery)

    @tailrec
    def doLoop(iteration: Int, weight: WeightFunction[Itemset], state: HotStart): LogisticWeight = {
      def sampleQuery(): Unit = {
        val overlap = if (iteration > 0) params.queryOverlap else 0
        if (iteration > 0) {
          queries(iteration - 1).copyToArray(queries(iteration), 0, overlap)
          queries(iteration).view(0, overlap).foreach { p => wg4ps.storeWeightInPlace(p, weight(p)) }
        }

        sample(miningProblem, weight, sampler0, rnd).take(params.querySize - overlap).zipWithIndex
          .foreach { case (itemset, j) => queries(iteration)(overlap + j) = itemset }
        scala.util.Sorting.quickSort(queries(iteration))(ByWeight)
      }

      def showNewExamplesToSCD(state: HotStart) =
        NewExamplesAppended(state.w, z, iteration * params.pairsPerQuery)

      def updateWeights() = {
        val m = (iteration + 1) * params.pairsPerQuery
        val trainingExamples = trainingPairs.view(0, m)
        SCD.optimize(
          loss, trainingExamples, params.scd.copy(lambda = params.scd.lambda * (iteration + 1)),
          state = if (iteration > 0) showNewExamplesToSCD(state) else state,
          m = Some(m), d = Some(d)
        )(rnd)
      }

      iteration match {
        case params.iterations =>
          val glf = weight.asInstanceOf[LogisticWeight]
          logTermination(iteration, glf)

          glf
        case _ =>
          logNewIteration(iteration)
          logAction { sampleQuery() } (s"Sampling ${params.querySize} itemsets to query")

          val query = queries(iteration)
          logAction { client.rank(query) } (s"Ranking ${params.querySize} itemsets")

          logAction {
            rankedPairs(query, params.querySize).zipWithIndex.foreach { case ((preferred, dispreferred), j) =>
              trainingPairs(iteration * params.pairsPerQuery + j) = new RankedPair(preferred, dispreferred, featureMap)
            }
          } (s"Generating ${params.pairsPerQuery} example pairs from ${params.querySize} itemsets")


          val (_, newState) = logAction { updateWeights() } (s"Updating $d weights")
          val learnedWeight = LogisticWeight(params.a, w, featureMap)

          logIterationOutcome(iteration, query, learnedWeight, client)

          doLoop(iteration + 1, learnedWeight, newState)
      }
    }

    doLoop(0, Uniform, HotStart(w, z))
  }

  private def sample(problem: EclatProblem, weight: WeightFunction[Itemset], sampler: EFlexicsWrapper, rnd: Random) =
    sampler.countThenSample(problem, weight, samplingConf, Left(countingConf), solutionWeight = Some(wg4ps.GetWeight))(rnd)

  private def rankedPairs(itemsets: Array[Itemset], k: Int): Iterator[(Itemset, Itemset)] =
    for (i <- Iterator.range(0, k - 1);
         j <- Iterator.range(i + 1, k))
      yield (itemsets(i), itemsets(j))
}

trait LewvitsLogging {
  protected val logger: Logger

  protected final def logAction[T](action: => T)(message: String): T = {
    logger.debug(message)
    val actionResult = action
    logger.debug(s" ${message.replace("ing", "ed")}")
    actionResult
  }

  protected final def logTask(dataset: Dataset[Set[Int]],
                              minsup: Int,
                              client: Ranker,
                              params: Parameters,
                              sampler: EFlexicsWrapper,
                              seed: Long): Unit = {
    logger.info(s"${dataset.name}, ${dataset.size}x${dataset.attributes.size}, minsup=$minsup")
    logger.info(s"  Ranker = $client")
    logger.info(s"Features = ${params.featureMap}")
    logger.info(s"  Params = iterations=${params.iterations}, k=${params.querySize}, l=${params.queryOverlap}")
    logger.info(f"     GLF = A=${params.a}%.3f")
    logger.info(f"     SCD = iterations=${params.scd.iterations}%s, lambda=${params.scd.lambda}%.2e")
    logger.info(s" Sampler = $sampler")
    logger.info(s"Rnd.seed = $seed")
  }

  protected final def logNewIteration(iteration: Int): Unit =
    logger.debug(s"Iteration $iteration")

  protected final def logIterationOutcome(iteration: Int,
                                          query: Array[Itemset],
                                          learnedWeight: LogisticWeight,
                                          client: Ranker): Unit =
    query.view.zipWithIndex.foreach { case (p, i) =>
      logger.info(f"$iteration%d;$i%d;${p.size}%d;${client.describe(p)}%s;${wg4ps.GetWeight(p)}%.6f;${learnedWeight(p)}%.6f;${p.sortedItems.mkString("+")}")
    }

  protected final def logTermination(iteration: Int, learnedWeight: LogisticWeight): Unit = {
    logger.info(s"Finished after $iteration iterations, returning $learnedWeight")
    logger.trace("\tFeature weights:")
    logger.trace(learnedWeight.featureMap.featureLabels.mkString(";"))
    logger.trace((0 until learnedWeight.featureMap.featureCount).map(j => f"${learnedWeight.weights(j)}%.6f").mkString(";"))
  }
}
