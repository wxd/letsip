package be.kuleuven

import be.kuleuven.lewvits.feature.{FeatureMap, Features}
import be.kuleuven.pmlib.itemsets.Itemset
import be.kuleuven.scd.{Example, ScdParameters, Vector, innerProduct}
import be.kuleuven.weightgen.BoundedWeightFunction

package object lewvits {
  val FeatureVectorKey = "lewits.features"

  def features(itemset: Itemset, featureMap: FeatureMap): scd.Vector =
    itemset.getMetadata[scd.Vector](FeatureVectorKey).getOrElse {
      val features0 = featureMap(itemset)
      itemset.storeMetadata(FeatureVectorKey, features0)

      features0
    }

  final case class Parameters(querySize: Int = 5,
                              queryOverlap: Int = 0,
                              iterations: Int = 10,
                              tilt: Double,
                              featureMap: Features,
                              scd: ScdParameters) {
    val pairsPerQuery: Int = querySize * (querySize - 1) / 2

    @inline def a: Double = 1.0 / tilt
  }

  final class RankedPair(val preferred: Itemset,
                         val dispreferred: Itemset,
                         featureMap: FeatureMap) extends Example {
    private val diffVector = {
      val dv = Array.ofDim[Double](featureMap.featureCount)

      val (f1, f2) = (features(preferred, featureMap), features(dispreferred, featureMap))
      (0 until featureMap.featureCount).foreach { i => dv(i) = f1(i) - f2(i) }

      dv
    }

    @inline override val length: Int = featureMap.featureCount
    @inline override def apply(j: Int): Double = diffVector(j)
    @inline override def label: Double = 1
  }

  final case class LogisticWeight(a: Double, weights: Vector, featureMap: FeatureMap) extends BoundedWeightFunction[Itemset] {
    override val tiltBound: Double = 1 / a

    override def weight(itemset: Itemset): Double = {
      val x = features(itemset, featureMap)
      require(x.length == weights.length, s"${weights.length} weights, but ${x.length} features")

      val wx = innerProduct(weights, x, Some(featureMap.featureCount))
      a + (1 - a) / (1 + Math.exp(-wx))
    }

    override def toString: String = s"LearnedLogisticWeight($a,$featureMap,${weights.length} weights)"
  }

  val ByWeight = Ordering.by(wg4ps.GetWeight).reverse
}
